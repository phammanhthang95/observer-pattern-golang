package main

import "fmt"

type Subject struct {
	observers []Observer
	state     int
}

func (s *Subject) Attach(o Observer) {
	s.observers = append(s.observers, o)
}

func (s *Subject) Notify() {
	for _, observer := range s.observers {
		observer.Update(s.state)
	}
}

func (s *Subject) SetState(state int) {
	s.state = state
	s.Notify()
}

type Observer interface {
	Update(int)
}

type ConcreteObserverA struct {
	state int
}

func (o *ConcreteObserverA) Update(state int) {
	o.state = state
	fmt.Println("ConcreteObserverA:", o.state)
}

type ConcreteObserverB struct {
	state int
}

func (o *ConcreteObserverB) Update(state int) {
	o.state = state
	fmt.Println("ConcreteObserverB:", o.state)
}

func main() {
	subject := Subject{}
	observerA := ConcreteObserverA{}
	observerB := ConcreteObserverB{}

	subject.Attach( &observerA)
	subject.Attach( &observerB)

	subject.SetState(1)
}